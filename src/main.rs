use std::f64::consts::{PI, E};
use std::io::{stdin, stdout, Write, self, BufRead};

use std::fs::File;
use std::path::Path;

mod hurricane;
mod constants;

use crate::hurricane::season::Season;
use crate::hurricane::storm::Storm;
use crate::constants::*;

fn main() {
    let mut season_data: Option<Season> = None;

    print!("{}[2J", 27 as char);
    stdout().flush().unwrap();
    println!("            HEg v{MAJOR_VERSION}.{MINOR_VERSION}");
    println!("            --------");

    loop {
        let user_selection = print_main_menu();
        match user_selection {
            0 => exit(),
            1 => {
                season_data = read_season()
            },
            2 => print_season(&season_data),
            3 => print_analysis(&season_data),
            4 => calculate_pdf(&season_data),
            _ => ()
        }  
    }
}

fn print_main_menu() -> u32 {
    let menu_options = [
        "Exit", 
        "Read Season Data",
        "Print Season Data",
        "Print Analysis",
        "Calculate PDF"
    ]; 
    
    println!();
    for (i, option) in menu_options.iter().enumerate() {
        println!("{}. {}", i, option);
    }

    println!();    

    loop {
        print!("Enter Selection: ");
        stdout().flush().unwrap();
        if let Some(user_selection) = get_stdin_usize() {
            println!();
            match user_selection {
                0 ..= 4 => break user_selection,
                _ => continue
            }  
        }
    }
}

fn exit() {
    println!("Thanks for using HEg v{MAJOR_VERSION}.{MINOR_VERSION}");
    std::process::exit(0);
}

fn read_season() -> Option<Season> {
    let mut user_input = String::new();
    print!("Please enter file name: ");
    stdout().flush().unwrap();
    let result = stdin().read_line(&mut user_input);
    if let Ok(_) = result {
        let filepath = user_input.trim();
        let path = Path::new(filepath);
        let display = path.display();
        let file = match File::open(&path) {
            Err(why) => panic!("couldn't open {}: {}", display, why),
            Ok(file) => file,
        };
        let mut season_data = Season::construct_season_data();
        let mut expected_num_storms: usize = 0;
        for (i, result_line) in io::BufReader::new(file).lines().enumerate() {
            if let Ok(line) = result_line {
                match i {
                    0 => season_data.year = line.parse::<u32>().unwrap(),
                    1 => {
                        expected_num_storms = line.parse::<usize>().unwrap();
                        season_data.storms = Vec::with_capacity(expected_num_storms);
                    },
                    _ => {
                        if let Some(storm_tuple) = parse_storm_tuple(line) {
                            if let Some(storm) = Storm::construct_storm(storm_tuple.0, storm_tuple.1, storm_tuple.2) {
                                season_data.add_storm(storm);
                            }
                        }
                    }
                }
            }
        }
        if season_data.storms.len() == expected_num_storms {
            return Some(season_data);
        }
    }
    None
}

fn parse_storm_tuple(line: String) -> Option<(u8, f64, f64)> {
    let mut values = line.split(',').map(|value| value.trim());
    let mut result = (0u8, 0.0, 0.0);
    if let Some(str1) = values.next() {
        if let Ok(year) = str1.parse::<u8>() {
            result.0 = year;
            if let Some(str1) = values.next() {
                if let Ok(wind_speed_mph) = str1.parse::<f64>() {
                    result.1 = wind_speed_mph;
                    if let Some(str1) = values.next() {
                        if let Ok(pressure_mbar) = str1.parse::<f64>() {
                            result.2 = pressure_mbar;
                            return Some(result);
                        }
                    }
                }
            }
        }
    }
    None
}

fn print_season(season_data: &Option<Season>) {
    if let Some(season_data) = season_data {
        println!("{} Hurricane Season Data ", season_data.year);
        println!("=============================");
        println!();
        println!("Month  Wind Speed       Max Pressure");
        println!("-----  -----------      -------------");
        for storm in &season_data.storms {
            println!("{:<2}             {:<3}              {:3}", storm.month, storm.wind_speed_mph, storm.pressure_mbar);
        }
    } else {
        println!("ERROR: Hurricane Season data was not read")
    }
}

fn print_analysis(season_data: &Option<Season>) {
    if let Some(season_data) = season_data {
        println!("{} Hurricane Season Data: ", season_data.year);
        println!("---------------------------");
        println!("Total number of tropical storms: {}", season_data.storms_by_category[0]);
        for i in 1..6 {
            println!("Total number of Category {} hurricanes: {}", i, season_data.storms_by_category[i]);
        }
        let wind_total_mph = season_data.storms.iter().map(|storm| storm.wind_speed_mph).sum::<f64>();
        let wind_total_kts = wind_total_mph * MPH_TO_KTS;
        println!("Cumulative wind speeds for the {} hurricane season are {wind_total_kts} knots", season_data.year);
    } else {
        println!("ERROR: Hurricane Season data was not read")
    }
}

fn calculate_pdf(season_data: &Option<Season>) {
    if let Some(season_data) = season_data {
        println!("  Probability Density Function (PDF) Analysis:");
        for i in 6..12 {
            println!("{i}: {:.2}", pdf(i as u32, season_data));
        }
    } else {
        println!("ERROR: Hurricane Season data was not read")
    }
}

fn pdf(month: u32, season_data: &Season) -> f64 {
    let mean = season_data.storms.len() as f64 / 6.0;
    let mut monthly_storm_totals = [0u32; 6];
    for storm in season_data.storms.iter() {
        monthly_storm_totals[storm.month as usize - 6] += 1;
    }
    let mut summation = 0.0;
    for x in monthly_storm_totals {
        summation += (x as f64 - mean).powi(2);
    }
    let std_dev = ((1.0 / 6.0) * summation).sqrt();
    (1.0 / (2.0 * PI * std_dev.powi(2)).sqrt()) * E.powf(-( ((month - 5) as f64 - mean).powi(2) / (2.0 * std_dev.powi(2)) ))
}

fn get_stdin_usize() -> Option<u32> {
    let mut user_selection = String::new();
    if let Ok(_) = stdin().read_line(&mut user_selection) {
        if let Ok(num) = user_selection.trim().parse::<u32>() {
            return Some(num);
        }
    }
    None
}
