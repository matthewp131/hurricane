pub const MAJOR_VERSION: u32 = 3;
pub const MINOR_VERSION: u32 = 0;
pub const MPH_TO_KTS: f64 = 0.86897624;