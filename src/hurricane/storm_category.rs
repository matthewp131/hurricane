use variant_count::VariantCount;
#[derive(VariantCount)]
#[derive(Copy, Clone)]
pub enum StormCategory {
    TS,
    Cat1,
    Cat2,
    Cat3,
    Cat4,
    Cat5
}