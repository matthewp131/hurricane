use crate::hurricane::storm_category::StormCategory;

pub struct Storm {    
    pub month: u8,
    pub wind_speed_mph: f64,
    pub pressure_mbar: f64,
    pub category: StormCategory
}

impl Storm {
    pub fn construct_storm(month: u8, wind_speed_mph: f64, pressure_mbar: f64) -> Option<Storm> {
        if wind_speed_mph >= 74.0 {
            let category = if wind_speed_mph >= 157.0 {
                StormCategory::Cat5
            } else if wind_speed_mph >= 130.0 {
                StormCategory::Cat4
            } else if wind_speed_mph >= 111.0 {
                StormCategory::Cat3
            } else if wind_speed_mph >= 96.0 {
                StormCategory::Cat2
            } else {
                StormCategory::Cat1
            };
            Some(Storm {
                category,
                wind_speed_mph,
                month,
                pressure_mbar
            })
        } else if wind_speed_mph >= 39.0 {
            Some(Storm {
                category: StormCategory::TS,
                wind_speed_mph,
                month,
                pressure_mbar
            })
        } else {
            None
        }
    }
}