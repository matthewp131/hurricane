use crate::hurricane::storm::Storm;
use crate::hurricane::storm_category::StormCategory;

pub struct Season {
    pub storms: Vec<Storm>,
    pub storms_by_category: [u32; StormCategory::VARIANT_COUNT],
    pub year: u32
}

impl Season {
    pub fn construct_season_data() -> Season {
        Season { 
            storms: Vec::new(), 
            storms_by_category: [0; StormCategory::VARIANT_COUNT], 
            year: 0 
        }
    }

    pub fn add_storm(&mut self, new_storm: Storm) {
        self.storms_by_category[new_storm.category as usize] += 1;
        self.storms.push(new_storm);
    }
}